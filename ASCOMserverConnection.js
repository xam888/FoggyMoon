var request = require('request');
var serverURL = '';
const EventEmitter = require("events");
var ee = new EventEmitter();

$('#serverURLBtn').click(function () {
    if ($('#serverURL').attr('disabled')) {
        $('#serverURLBtn').html('Set connection URL');
        $('#serverURL').attr('disabled', false);
        $('#serverURL').val('');
        $('#serverAlert').html("Connection not established")
        $('#serverAlert').removeClass("alert-success")
        $('#serverAlert').addClass("alert-danger")
        $('#avaliableDevices').find("button").attr("disabled", true);
        $('#avaliableDevices').find("button").removeClass("btn-success");
        $('#avaliableDevices').find("button").addClass("btn-danger");
        $('#avaliableDevices').find("ul").empty();
        $('#streamDataBtn').attr('disabled', true);
        $('.spinner').hide();
        return;
    }
    $('#serverURLBtn').attr("disabled", true);
    request($('#serverURL').val(), function (err, res, body) {
        $('#serverURLBtn').attr("disabled", false);
        if (err) {
            $('#serverAlert').html("Connection can't be established to the specified URL")
            $('#serverAlert').removeClass("alert-success")
            $('#serverAlert').addClass("alert-danger")
        }
        else if (res.statusCode === 200) {
            var splittedString = body.split("Available devices:</b></p>")
            avaliableDevices = splittedString[1].split("<br>");
            avaliableDevices.forEach((deviceName) => {
                $('#avaliableDevices').find("ul").append('<li><a href="#">' + deviceName + '</a></li>');
            })
            $('#avaliableDevices').find("button").attr("disabled", false);
            $('#avaliableDevices').find("button").removeClass("btn-danger");
            $('#avaliableDevices').find("button").addClass("btn-success");
            serverURL = $('#serverURL').val();
            $('#serverURL').attr("disabled", true);
            $('#serverAlert').html("Connection established")
            $('#serverURLBtn').html("Change URL");
            $('#serverAlert').removeClass("alert-danger")
            $('#serverAlert').addClass("alert-success")
            $('#streamDataBtn').attr('disabled', false);
        }
    });
});

module.exports.getServerURL = function () {
    return serverURL;
};
module.exports.ee = ee;
