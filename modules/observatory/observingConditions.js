const { ipcRenderer } = require('electron');
var request = require('request');
const EventEmitter = require("events");
var ASCOMServer = require('./ASCOMserverConnection');
var conditions = require('./conditionsConfig.json');
var ee = new EventEmitter();
var gatherData = true;


ipcRenderer.on('openObCon', (event, arg) => {
    $('#observingConditions').show();
});

$('#dataIntervalBtn').click(() => {
    if ($('#dataInterval').attr('disabled')) {
        $('#dataInterval').attr('disabled', false);
        $('#dataIntervalBtn').html('Set interval');
    }
    else {
        $('#dataIntervalBtn').html('Change interval');
        $('#dataInterval').attr('disabled', true);
        dataGetInterval = $('#dataInterval').val();
    }
});

function checkCollectingData(){
    if(!gatherData){
        $('#conditionsList').children().each(function(el){
            var splitedString = $(this).children('p').text().split(': ');
            $(this).children('p').text(splitedString[0] + ': no data');
        });
        gatherData = true;
        return false
    }
    else{
        return true;
    }
}

function getDataFromWeatherStation(counter) {
    var con = conditions.conditions[counter];
    request(ASCOMServer.getServerURL() + '/API/V1/ObservingConditions/0/' + con.name, (err, response, body) => {
        if (!err && response.statusCode == 200) {
            if (body) {
                var jsonCon = JSON.parse(body);
                typeof (jsonCon.Value) ? jsonCon.Value = jsonCon.Value.toFixed(2) : null;
                if(!(con.name === 'skyQuality')){
                $('#' + con.name).html(con.label + ": " + jsonCon.Value + con.unicode + con.unit);
            }
            else{
                $('#' + con.name).html(con.label + ": " + jsonCon.Value + con.unit + con.unicode);
            }
                if (conditions.conditions.length - 1 === counter) {
                    counter = 0;
                    setTimeout(function () {
                        if(!checkCollectingData()) return;
                        getDataFromWeatherStation(counter);
                    }, conditions.dataGetInterval);
                }
                else {
                    counter++
                      if(!checkCollectingData()) return;
                    getDataFromWeatherStation(counter);
                }
                // if (temp.Value > 5);
                // ee.emit("domeControl", 'close');
            }
        }
    });
}
$('#streamDataBtn').click(() => {
    if ($('.spinner').is(':visible')) {
        gatherData = false;
        $('.spinner').hide();
        $('#streamingInfo').removeClass('alert-success');
        $('#streamingInfo').addClass('alert-warning');
         $('#streamingInfo').html('Data streaming stopped');
        $('#streamDataBtn').html('Start weather data streaming');
    }
    else{
    getDataFromWeatherStation(0);
    $('.spinner').show();
    $('#streamingInfo').show();
    $('#streamDataBtn').html('Stop weather data streaming');
    }
});

conditions.conditions.forEach((el) => {
    $('#conditionsList').append("<div style='display:flex'><i class='wi " + el.class + "'></i><p id='" + el.name + "'style='margin-right:3em; margin-left:0.5em'>" + el.label + ": " + el.value + "</p></div>")
});


module.exports.ee = ee;


