/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

'use strict';

const {dialog} = require('electron').remote
var fs = require('fs');
var width = 300;
var height = 225;
var interval = 100;
var tolerance = 100;

var videoElement = document.querySelector('video'),
  videoSelect = document.querySelector('select#videoSource'),
  selectors = [videoSelect],
  moveDetection = $('#moveDetection'),
  canvas = document.createElement('canvas'),
  ctx = canvas.getContext('2d'),
  currentPixels,
  lastPixels,
  imageToSave,
  saveFiles = false,
  allStackPixels = [];

function gotDevices(deviceInfos) {
  // Handles being called several times to update labels. Preserve values.
  var values = selectors.map(function (select) {
    return select.value;
  });
  selectors.forEach(function (select) {
    while (select.firstChild) {
      select.removeChild(select.firstChild);
    }
  });
  for (var i = 0; i !== deviceInfos.length; ++i) {
    var deviceInfo = deviceInfos[i];
    var option = document.createElement('option');
    option.value = deviceInfo.deviceId;
    if (deviceInfo.kind === 'videoinput') {
      option.text = deviceInfo.label || 'camera ' + (videoSelect.length + 1);
      videoSelect.appendChild(option);
    }
  }
  selectors.forEach(function (select, selectorIndex) {
    if (Array.prototype.slice.call(select.childNodes).some(function (n) {
      return n.value === values[selectorIndex];
    })) {
      select.value = values[selectorIndex];
    }
  });
}

navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);

// Attach audio output device to video element using device/sink ID.
function attachSinkId(element, sinkId) {
  if (typeof element.sinkId !== 'undefined') {
    element.setSinkId(sinkId)
      .then(function () {
        console.log('Success, audio output device attached: ' + sinkId);
      })
      .catch(function (error) {
        var errorMessage = error;
        if (error.name === 'SecurityError') {
          errorMessage = 'You need to use HTTPS for selecting audio output ' +
            'device: ' + error;
        }
        console.error(errorMessage);
        // Jump back to first output device in the list as it's the default.
        audioOutputSelect.selectedIndex = 0;
      });
  } else {
    console.warn('Browser does not support output device selection.');
  }
}

function changeAudioDestination() {
  var audioDestination = audioOutputSelect.value;
  attachSinkId(videoElement, audioDestination);
}

function gotStream(stream) {
  window.stream = stream; // make stream available to console
  videoElement.srcObject = stream;
  canvas.width = width;
  canvas.height = height;
  $("#canvasContainer").append(canvas);
  setInterval(function () {
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
    imageToSave = canvas.toDataURL('image/png');
    var base64Data = imageToSave.replace(/^data:image\/png;base64,/, "");
    lastPixels = currentPixels;
    currentPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
    if(saveFiles){
      fs.writeFile(folderDirforFrames+"\\hahaha"+Math.random()+".png", base64Data, 'base64');
      allStackPixels.push(currentPixels.data);
    }
    var same = equal(lastPixels, currentPixels, tolerance);
    if(!same){
      moveDetection.removeClass('alert-success');
      moveDetection.addClass('alert-danger');
      moveDetection.html('Motion detected!')
    }
    else{
      moveDetection.removeClass('alert-danger');
      moveDetection.addClass('alert-success');
      moveDetection.html("The camera doesn't detect motion")
    }
  }, interval);
  // Refresh button list in case labels have become available
  return navigator.mediaDevices.enumerateDevices();
}

function equal(a, b, tolerance) {
  var
    aData = a && a.data || [],
    bData = b.data,
    length = aData.length,
    i;

  tolerance = tolerance || 0;

  for (i = length; i--;) {
    if (aData[i] !== bData[i] && Math.abs(aData[i] - bData[i]) > tolerance) {
      return false;
    }
  }
  return true;
}

function start() {
  if (window.stream) {
    window.stream.getTracks().forEach(function (track) {
      track.stop();
    });
  }
  var videoSource = videoSelect.value;
  var constraints = {
    video: { deviceId: videoSource ? { exact: videoSource } : undefined }
  };
  navigator.mediaDevices.getUserMedia(constraints).
    then(gotStream).then(gotDevices).catch(handleError);
}

videoSelect.onchange = start;

start();

function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}


