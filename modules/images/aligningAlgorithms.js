var gm = require('gm');

module.exports.getPlanetCentroid = function (image) {
    var threshold = 50;
    var data = image.data, counter = 0, planetPixels = []; initData = [];
    // data.forEach((pixel)=>{
    //     initData.push(pixel)
    // });
    for (var i = 0; i < image.data.length; i = i + 4) {
        if (data[i] > threshold && data[i + 1] > threshold && data[i + 2] > threshold) {
            planetPixels.push(i);
            // data[i] = 255, data[i+1] = 255, data[i+2] = 0; data[i+3] = 255;
        }
        counter++;
    }
    var firstPixel = planetPixels[0]/4;
    var lastPixel = planetPixels[planetPixels.length-1]/4
    return [firstPixel, lastPixel]
}

module.exports.getXYForPixel = function(imageSize, pixels){
    var yPositionFirstPixel = parseInt(pixels[0]/imageSize.width);
    var xPositionFirstPixel = pixels[0] % imageSize.width;
    var xPositionLastPixel = pixels[1] % imageSize.width;
    var yPositionLastPixel = parseInt(pixels[1]/imageSize.width);
    var centroid = {
        x: parseInt((xPositionFirstPixel + xPositionLastPixel)/2),
        y: parseInt((yPositionFirstPixel + yPositionLastPixel)/2)
    }
    return centroid;
}


