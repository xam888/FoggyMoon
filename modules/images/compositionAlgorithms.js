module.exports.sum = function (index, allStackPixels) {
  var sum = 0;
  allStackPixels.forEach((stack) => {
    sum += stack[index];
  });
  return sum;
}

module.exports.average = function (index, allStackPixels) {
  var sum = 0, average;
  allStackPixels.forEach((stack) => {
    sum += stack[index];
  });
  average = sum / allStackPixels.length
  return average;
}

module.exports.median = function (index, images) {
  var median, arrayForIndex = [];
  images.forEach((image) => {
    var imageData = image.ctx.getImageData(0, 0, image.size.width, image.size.height)
    arrayForIndex.push(imageData[index]);
  });
  arrayForIndex.sort((a, b) => a - b);
  median = (arrayForIndex[(arrayForIndex.length - 1) >> 1] + arrayForIndex[arrayForIndex.length >> 1]) / 2
  return median;
}