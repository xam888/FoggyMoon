var compositions = require('./compositionAlgorithms');
var aligning = require('./aligningAlgorithms');
var fs = require('fs');
var gm = require('gm')
const {dialog} = require('electron').remote
var images = [];
var folderDirforFrames;
var mainCentroid;
var aligningCentroid;

Object.keys(compositions).forEach((composition)=>{
  var option = $('<option></option>');
  option.value = composition;
  option.html(composition);
  $('#compositionSelect').append(option);  
})

$('#choosePhoto').click(() => {
  dialog.showOpenDialog({
        properties: ['openFiles', 'multiSelections'],
      filters: [
        {name: 'Images', extensions: ['jpg', 'png']}
        ]
  }, (paths) => {
    paths.forEach((path)=>{
      readImage(path)
    });
  });
});


function readImage(path) {
  gm(path).identify(function (err, res) {
    fs.readFile(path, null, (error, result) => {
      var data;
      var base64Image = new Buffer(result, 'binary').toString('base64');
      base64Image = 'data:image/jpg;base64,' + base64Image;
      var a = $('<a href="#"  data-html="true"  data-trigger="hover"  rel="popover" data-placement="right" data-original-title="Image Info"></a>');
      var canvas = document.createElement('canvas');
      a.append(canvas);
      var content = $('<div style="font-size:0.8em"></div>')
      var keys = Object.keys(res);
      for(var i = 0; i < keys.length; i++){
        var p = $('<p style="margin:0 0 0"><b>'+ keys[i] +  ": </b>" + res[keys[i]] +'</p>')
        content.append(p)
      }
      a.popover({
        content: content.html()
      });
      a.css("display", "block").css("width", res.size.width).css("height", res.size.height).css("cursor", "default");      
      $('#originalImage').append('<br>');
      $('#originalImage').append(a);
      canvas.width = res.size.width;
      canvas.height = res.size.height;
      var ctx = canvas.getContext('2d');
      images.push({ctx: ctx, size: res.size, path:path});
      var image = new Image();
      image.src = base64Image;
      image.onload = function () {
        var ctx2 = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0);
      };
    });
  });
}

$('#alignPhotos').click(() => {
  images.forEach((image, index) => {
    var data = image.ctx.getImageData(0, 0, image.size.width, image.size.height);
    var marginPixels = aligning.getPlanetCentroid(data);
    centroidXY = aligning.getXYForPixel(image.size, marginPixels);
    index === 0 ? mainCentroid = centroidXY : aligningCentroid = centroidXY;
    if(index === 1){
    var moveX = mainCentroid.x - aligningCentroid.x
    var moveY = mainCentroid.y - aligningCentroid.y
      gm(image.path).roll(moveX, moveY).write('C:/Users/Maks/Desktop/git/FoggyMoon/icons/new.jpg', (err)=>{
        if(err) console.info(err);
      });
    }
    // var canvas = document.createElement('canvas');
    // canvas.width = image.size.width;
    // canvas.height = image.size.height;
    // var ctx = canvas.getContext('2d');
    // var imageData = ctx.createImageData(image.size.width, image.size.height);
    // imageData.data.set(centroid);
    // ctx.putImageData(imageData, 0, 0)
    // $('#originalImage').append(canvas);
  });
})


$('#stackImages').click(() => {
    var data = images[0].ctx.getImageData(0, 0, images[0].size.width, images[0].size.height);
    var result = data.data.map((value, index, array) => {
      return compositions.median(index, images);
    });
    var canvas = document.createElement('canvas');
    canvas.width = images[0].size.width,
    canvas.height = iamges[0].size.height;
    var ctxNew = canvas.getContext('2d');
    var imageData = ctxNew.createImageData(images[0].size.width, images[0].size.height);
    imageData.data.set(result);
    ctxNew.putImageData(imageData, 0, 0);
    var imageToSave = canvas.toDataURL('image/png');
    var base64Data = imageToSave.replace(/^data:image\/png;base64,/, "");
    fs.writeFile('C:/Users/Maks/Desktop/git/FoggyMoon/icons/' + "\\aaaaaaaaaaaaaaaa" + Math.random() + ".png", base64Data, 'base64');
});

$('#inicateFrameSaveFolder').click(() => {
  dialog.showOpenDialog({
    properties: ['openDirectory']
  }, (folder)=>{
    folderDirforFrames = folder;
    folder ? $('#startRecording').attr('disabled', false) : null
  });
});