# "Foogy Moon" - aplikacja dla astronomów

## Oczekiwane funkcjonalności:
    1. Zarządzanie obserwatorium
    2. Zarządzanie wykonywaniem zdjęć
    3. Zarządzanie przetwarzaniem zdjęć
    4. Zarządzanie teleskopem

## Obsługiwane os-y:
    1. Windows
    2. Linux
    3. Max OSX

## Wykorzystywane frameworki:
    1. Node.js
    2. Electron
    
## Wykorzystywane biblioteki:   

1. jQuery
2. Bootstrap
3. [gPhoto2/libgphoto2](http://gphoto.org)  (zdalna kontrola DSLR)
4. [graphicsMagic](http://aheckmann.github.io/gm) (przetwarzanie zdjęć)


## Dodatkowe wymagania:
    1. Obsługa sterowników ASCOM
