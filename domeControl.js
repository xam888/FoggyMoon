var http = require('request');
var renderer = require('./renderer');
var observingConditions = require('./observingConditions');
var shutterStatus = 'closed' //TODO

$('#domeShutterBtn').click(function(){
    $('#abortSlew').attr('disabled', false)
    shutterStatus = 'opened';
    $('#domeShutterBtn').html('Opening...');
    $('#domeShutterBtn').attr('disabled', true);
    renderer.notification('WARNING: Dome is moving!', 'Shutter is opening...');
});

$('#abortSlew').click(function(){
    $('#abortSlew').attr('disabled', true)
    shutterStatus = 'middleOpen';
    $('#domeShutterBtn').attr('disabled', false);
    $('#domeShutterBtn').html('Open');
    renderer.notification('WARNING!', 'Shutter is middle-open');
});

// observingConditions.ee.on("domeControl", function (param) {
//     if (param === 'close') {
//         request({
//             url: 'http://localhost:11111/API/V1/Dome/0/closeshutter',
//             method: 'PUT',
//             json: {},
//             function(res, error) {
//                 console.info(res, error)
//             }
//         });
//     }
// });
