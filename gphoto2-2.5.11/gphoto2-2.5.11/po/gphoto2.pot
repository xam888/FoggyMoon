# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Lutz Müller and others
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: gphoto-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2016-11-20 22:23+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: gphoto2/actions.c:174
#, c-format
msgid "Number of files in folder '%s': %i\n"
msgstr ""

#: gphoto2/actions.c:195
#, c-format
msgid "There is %d folder in folder '%s'.\n"
msgid_plural "There are %d folders in folder '%s'.\n"
msgstr[0] ""
msgstr[1] ""

#: gphoto2/actions.c:244
#, c-format
msgid "There is no file in folder '%s'.\n"
msgstr ""

#: gphoto2/actions.c:247
#, c-format
msgid "There is %d file in folder '%s'.\n"
msgid_plural "There are %d files in folder '%s'.\n"
msgstr[0] ""
msgstr[1] ""

#: gphoto2/actions.c:269
#, c-format
msgid "Information on file '%s' (folder '%s'):\n"
msgstr ""

#: gphoto2/actions.c:271
#, c-format
msgid "File:\n"
msgstr ""

#: gphoto2/actions.c:273 gphoto2/actions.c:305 gphoto2/actions.c:321
#, c-format
msgid "  None available.\n"
msgstr ""

#: gphoto2/actions.c:276 gphoto2/actions.c:308
#, c-format
msgid "  Mime type:   '%s'\n"
msgstr ""

#: gphoto2/actions.c:278 gphoto2/actions.c:310
#, c-format
msgid "  Size:        %lu byte(s)\n"
msgstr ""

#: gphoto2/actions.c:280 gphoto2/actions.c:312
#, c-format
msgid "  Width:       %i pixel(s)\n"
msgstr ""

#: gphoto2/actions.c:282 gphoto2/actions.c:314
#, c-format
msgid "  Height:      %i pixel(s)\n"
msgstr ""

#: gphoto2/actions.c:284 gphoto2/actions.c:316
#, c-format
msgid "  Downloaded:  %s\n"
msgstr ""

#: gphoto2/actions.c:285 gphoto2/actions.c:317 gphoto2/actions.c:329
#: gphoto2/actions.c:682 gphoto2/actions.c:684 gphoto2/actions.c:716
#: gphoto2/actions.c:719 gphoto2/actions.c:722 gphoto2/actions.c:725
#: gphoto2/actions.c:728 gphoto2/actions.c:1803 gphoto2/actions.c:2046
msgid "yes"
msgstr ""

#: gphoto2/actions.c:285 gphoto2/actions.c:317 gphoto2/actions.c:329
#: gphoto2/actions.c:682 gphoto2/actions.c:684 gphoto2/actions.c:716
#: gphoto2/actions.c:719 gphoto2/actions.c:722 gphoto2/actions.c:725
#: gphoto2/actions.c:728 gphoto2/actions.c:1797 gphoto2/actions.c:2040
msgid "no"
msgstr ""

#: gphoto2/actions.c:287
#, c-format
msgid "  Permissions: "
msgstr ""

#: gphoto2/actions.c:290
#, c-format
msgid "read/delete"
msgstr ""

#: gphoto2/actions.c:292
#, c-format
msgid "read"
msgstr ""

#: gphoto2/actions.c:294
#, c-format
msgid "delete"
msgstr ""

#: gphoto2/actions.c:296
#, c-format
msgid "none"
msgstr ""

#: gphoto2/actions.c:300
#, c-format
msgid "  Time:        %s"
msgstr ""

#: gphoto2/actions.c:303
#, c-format
msgid "Thumbnail:\n"
msgstr ""

#: gphoto2/actions.c:319
#, c-format
msgid "Audio data:\n"
msgstr ""

#: gphoto2/actions.c:324
#, c-format
msgid "  Mime type:  '%s'\n"
msgstr ""

#: gphoto2/actions.c:326
#, c-format
msgid "  Size:       %lu byte(s)\n"
msgstr ""

#: gphoto2/actions.c:328
#, c-format
msgid "  Downloaded: %s\n"
msgstr ""

#: gphoto2/actions.c:504
msgid "Could not parse EXIF data."
msgstr ""

#: gphoto2/actions.c:508
#, c-format
msgid "EXIF tags:"
msgstr ""

#: gphoto2/actions.c:511
msgid "Tag"
msgstr ""

#: gphoto2/actions.c:513
msgid "Value"
msgstr ""

#: gphoto2/actions.c:534
#, c-format
msgid "EXIF data contains a thumbnail (%i bytes)."
msgstr ""

#: gphoto2/actions.c:543
msgid "gphoto2 has been compiled without EXIF support."
msgstr ""

#: gphoto2/actions.c:561
#, c-format
msgid "Number of supported cameras: %i\n"
msgstr ""

#: gphoto2/actions.c:562
#, c-format
msgid "Supported cameras:\n"
msgstr ""

#: gphoto2/actions.c:575
#, c-format
msgid "\t\"%s\" (TESTING)\n"
msgstr ""

#: gphoto2/actions.c:578
#, c-format
msgid "\t\"%s\" (EXPERIMENTAL)\n"
msgstr ""

#: gphoto2/actions.c:583
#, c-format
msgid "\t\"%s\"\n"
msgstr ""

#: gphoto2/actions.c:627
#, c-format
msgid "Devices found: %i\n"
msgstr ""

#: gphoto2/actions.c:628
#, c-format
msgid ""
"Path                             Description\n"
"--------------------------------------------------------------\n"
msgstr ""

#: gphoto2/actions.c:661 gphoto2/actions.c:666
#, c-format
msgid "%-30s %-16s\n"
msgstr ""

#: gphoto2/actions.c:661
msgid "Model"
msgstr ""

#: gphoto2/actions.c:661
msgid "Port"
msgstr ""

#: gphoto2/actions.c:662
#, c-format
msgid "----------------------------------------------------------\n"
msgstr ""

#: gphoto2/actions.c:680
#, c-format
msgid "Abilities for camera             : %s\n"
msgstr ""

#: gphoto2/actions.c:681
#, c-format
msgid "Serial port support              : %s\n"
msgstr ""

#: gphoto2/actions.c:683
#, c-format
msgid "USB support                      : %s\n"
msgstr ""

#: gphoto2/actions.c:686
#, c-format
msgid "Transfer speeds supported        :\n"
msgstr ""

#: gphoto2/actions.c:688
#, c-format
msgid "                                 : %i\n"
msgstr ""

#: gphoto2/actions.c:691
#, c-format
msgid "Capture choices                  :\n"
msgstr ""

#: gphoto2/actions.c:693
#, c-format
msgid "                                 : Image\n"
msgstr ""

#: gphoto2/actions.c:697
#, c-format
msgid "                                 : Video\n"
msgstr ""

#: gphoto2/actions.c:701
#, c-format
msgid "                                 : Audio\n"
msgstr ""

#: gphoto2/actions.c:705
#, c-format
msgid "                                 : Preview\n"
msgstr ""

#: gphoto2/actions.c:709
#, c-format
msgid "                                 : Trigger Capture\n"
msgstr ""

#: gphoto2/actions.c:713
#, c-format
msgid ""
"                                 : Capture not supported by the driver\n"
msgstr ""

#: gphoto2/actions.c:715
#, c-format
msgid "Configuration support            : %s\n"
msgstr ""

#: gphoto2/actions.c:717
#, c-format
msgid "Delete selected files on camera  : %s\n"
msgstr ""

#: gphoto2/actions.c:720
#, c-format
msgid "Delete all files on camera       : %s\n"
msgstr ""

#: gphoto2/actions.c:723
#, c-format
msgid "File preview (thumbnail) support : %s\n"
msgstr ""

#: gphoto2/actions.c:726
#, c-format
msgid "File upload support              : %s\n"
msgstr ""

#: gphoto2/actions.c:743
#, c-format
msgid ""
"Ports must look like 'serial:/dev/ttyS0' or 'usb:', but '%s' is missing a "
"colon so I am going to guess what you mean."
msgstr ""

#: gphoto2/actions.c:777
#, c-format
msgid ""
"The port you specified ('%s') can not be found. Please specify one of the "
"ports found by 'gphoto2 --list-ports' and make sure the spelling is correct "
"(i.e. with prefix 'serial:' or 'usb:')."
msgstr ""

#: gphoto2/actions.c:810
#, c-format
msgid "About the camera driver:"
msgstr ""

#: gphoto2/actions.c:823
#, c-format
msgid "Camera summary:"
msgstr ""

#: gphoto2/actions.c:836
#, c-format
msgid "Camera manual:"
msgstr ""

#: gphoto2/actions.c:853
#, c-format
msgid "You can only specify speeds for serial ports."
msgstr ""

#: gphoto2/actions.c:903
msgid "OS/2 port by Bart van Leeuwen\n"
msgstr ""

#: gphoto2/actions.c:907
#, c-format
msgid ""
"gphoto2 %s\n"
"\n"
"Copyright (c) 2000-%d Lutz Mueller and others\n"
"%s\n"
"gphoto2 comes with NO WARRANTY, to the extent permitted by law. You may\n"
"redistribute copies of gphoto2 under the terms of the GNU General Public\n"
"License. For more information about these matters, see the files named "
"COPYING.\n"
"\n"
"This version of gphoto2 is using the following software versions and "
"options:\n"
msgstr ""

#: gphoto2/actions.c:1020
msgid "Could not open 'movie.mjpg'."
msgstr ""

#: gphoto2/actions.c:1027
#, c-format
msgid "Capturing preview frames as movie to '%s'. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1031
#, c-format
msgid "Capturing preview frames as movie to '%s' for %d seconds.\n"
msgstr ""

#: gphoto2/actions.c:1036
#, c-format
msgid "Capturing %d preview frames as movie to '%s'.\n"
msgstr ""

#: gphoto2/actions.c:1046
msgid "Movie capture error... Exiting."
msgstr ""

#: gphoto2/actions.c:1051
#, c-format
msgid "Movie capture error... Unhandled MIME type '%s'."
msgstr ""

#: gphoto2/actions.c:1058
#, c-format
msgid "Ctrl-C pressed ... Exiting.\n"
msgstr ""

#: gphoto2/actions.c:1072
#, c-format
msgid "Movie capture finished (%d frames)\n"
msgstr ""

#: gphoto2/actions.c:1102
#, c-format
msgid "Waiting for events from camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1108
#, c-format
msgid "Waiting for %d frames from the camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1113
#, c-format
msgid ""
"Waiting for %d milliseconds for events from camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1118
#, c-format
msgid "Waiting for %d seconds for events from camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1121
#, c-format
msgid "Waiting for %d events from camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1125
#, c-format
msgid "Waiting for %s event from camera. Press Ctrl-C to abort.\n"
msgstr ""

#: gphoto2/actions.c:1169 gphoto2/actions.c:1183 gphoto2/actions.c:1199
#: gphoto2/actions.c:1237 gphoto2/actions.c:1245
#, c-format
msgid "event found, stopping wait!\n"
msgstr ""

#: gphoto2/actions.c:1209 gphoto2/main.c:835
msgid "Could not set folder."
msgstr ""

#: gphoto2/actions.c:1215 gphoto2/main.c:850
msgid "Could not get image."
msgstr ""

#: gphoto2/actions.c:1222 gphoto2/main.c:857
msgid "Buggy libcanon.so?"
msgstr ""

#: gphoto2/actions.c:1232 gphoto2/main.c:869
msgid "Could not delete image."
msgstr ""

#: gphoto2/actions.c:1264
#, c-format
msgid "Getting storage information not supported for this camera.\n"
msgstr ""

#: gphoto2/actions.c:1279
#, c-format
msgid "Read-Write"
msgstr ""

#: gphoto2/actions.c:1282
#, c-format
msgid "Read-Only"
msgstr ""

#: gphoto2/actions.c:1285
#, c-format
msgid "Read-only with delete"
msgstr ""

#: gphoto2/actions.c:1288 gphoto2/actions.c:1298
#, c-format
msgid "Unknown"
msgstr ""

#: gphoto2/actions.c:1301
#, c-format
msgid "Fixed ROM"
msgstr ""

#: gphoto2/actions.c:1304
#, c-format
msgid "Removable ROM"
msgstr ""

#: gphoto2/actions.c:1307
#, c-format
msgid "Fixed RAM"
msgstr ""

#: gphoto2/actions.c:1310
#, c-format
msgid "Removable RAM"
msgstr ""

#: gphoto2/actions.c:1320
#, c-format
msgid "Undefined"
msgstr ""

#: gphoto2/actions.c:1323
#, c-format
msgid "Generic Flat"
msgstr ""

#: gphoto2/actions.c:1326
#, c-format
msgid "Generic Hierarchical"
msgstr ""

#: gphoto2/actions.c:1329
#, c-format
msgid "Camera layout (DCIM)"
msgstr ""

#: gphoto2/actions.c:1367
#, c-format
msgid "Overriding USB vendor/product id 0x%x/0x%x with 0x%x/0x%x"
msgstr ""

#: gphoto2/actions.c:1435
msgid ""
"ALWAYS INCLUDE THE FOLLOWING LINES WHEN SENDING DEBUG MESSAGES TO THE "
"MAILING LIST:"
msgstr ""

#: gphoto2/actions.c:1450
#, c-format
msgid "%s has been compiled with the following options:"
msgstr ""

#: gphoto2/actions.c:1590
#, c-format
msgid "%s not found in configuration tree."
msgstr ""

#: gphoto2/actions.c:1637
#, c-format
msgid "Failed to retrieve value of text widget %s."
msgstr ""

#: gphoto2/actions.c:1654
#, c-format
msgid "Failed to retrieve values of range widget %s."
msgstr ""

#: gphoto2/actions.c:1666
#, c-format
msgid "Failed to retrieve values of toggle widget %s."
msgstr ""

#: gphoto2/actions.c:1678
#, c-format
msgid "Failed to retrieve values of date/time widget %s."
msgstr ""

#: gphoto2/actions.c:1687
msgid "Use 'now' as the current time when setting.\n"
msgstr ""

#: gphoto2/actions.c:1709
#, c-format
msgid "Failed to retrieve values of radio widget %s."
msgstr ""

#: gphoto2/actions.c:1753
#, c-format
msgid "Property %s is read only."
msgstr ""

#: gphoto2/actions.c:1767 gphoto2/actions.c:2010
#, c-format
msgid "Failed to set the value of text widget %s to %s."
msgstr ""

#: gphoto2/actions.c:1777 gphoto2/actions.c:2020
#, c-format
msgid "The passed value %s is not a floating point value."
msgstr ""

#: gphoto2/actions.c:1782 gphoto2/actions.c:2025
#, c-format
msgid "The passed value %f is not within the expected range %f - %f."
msgstr ""

#: gphoto2/actions.c:1788 gphoto2/actions.c:2031
#, c-format
msgid "Failed to set the value of range widget %s to %f."
msgstr ""

#: gphoto2/actions.c:1797 gphoto2/actions.c:2040
msgid "off"
msgstr ""

#: gphoto2/actions.c:1798 gphoto2/actions.c:2041
msgid "false"
msgstr ""

#: gphoto2/actions.c:1803 gphoto2/actions.c:2046
msgid "on"
msgstr ""

#: gphoto2/actions.c:1804 gphoto2/actions.c:2047
msgid "true"
msgstr ""

#: gphoto2/actions.c:1809 gphoto2/actions.c:2052
#, c-format
msgid "The passed value %s is not a valid toggle value."
msgstr ""

#: gphoto2/actions.c:1815 gphoto2/actions.c:2058
#, c-format
msgid "Failed to set values %s of toggle widget %s."
msgstr ""

#: gphoto2/actions.c:1827
msgid "now"
msgstr ""

#: gphoto2/actions.c:1839 gphoto2/actions.c:2071
#, c-format
msgid "The passed value %s is neither a valid time nor an integer."
msgstr ""

#: gphoto2/actions.c:1847 gphoto2/actions.c:2078
#, c-format
msgid "Failed to set new time of date/time widget %s to %s."
msgstr ""

#: gphoto2/actions.c:1894 gphoto2/actions.c:1961 gphoto2/actions.c:2108
#, c-format
msgid "Choice %s not found within list of choices."
msgstr ""

#: gphoto2/actions.c:1902 gphoto2/actions.c:2116
#, c-format
msgid "The %s widget is not configurable."
msgstr ""

#: gphoto2/actions.c:1912 gphoto2/actions.c:1983 gphoto2/actions.c:2126
#, c-format
msgid "Failed to set new configuration value %s for configuration entry %s."
msgstr ""

#: gphoto2/actions.c:1973
#, c-format
msgid ""
"The %s widget has no indexed list of choices. Use --set-config-value instead."
msgstr ""

#: gphoto2/foreach.c:260
#, c-format
msgid ""
"Bad file number. You specified %i, but there are only %i files available in "
"'%s' or its subfolders. Please obtain a valid file number from a file "
"listing first."
msgstr ""

#: gphoto2/foreach.c:285
#, c-format
msgid "There are no files in folder '%s'."
msgstr ""

#: gphoto2/foreach.c:291
#, c-format
msgid ""
"Bad file number. You specified %i, but there is only 1 file available in "
"'%s'."
msgstr ""

#: gphoto2/foreach.c:299
#, c-format
msgid ""
"Bad file number. You specified %i, but there are only %i files available in "
"'%s'. Please obtain a valid file number from a file listing first."
msgstr ""

#: gphoto2/gp-params.c:66
#, c-format
msgid "*** Error ***              \n"
msgstr ""

#: gphoto2/gp-params.c:241
#, c-format
msgid "Press any key to continue.\n"
msgstr ""

#: gphoto2/gp-params.c:263
#, c-format
msgid "Not enough memory."
msgstr ""

#: gphoto2/gphoto2-cmd-capture.c:211
msgid "Operation cancelled"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:57
msgid "</B/24>Continue"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:57
msgid "</B16>Cancel"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:63
msgid "<C></5>Error"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:65
msgid "Could not set configuration:"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:109
msgid "Exit"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:111
msgid "Back"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:256
msgid "Time: "
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:315 gphoto2/gphoto2-cmd-config.c:343
#: gphoto2/gphoto2-cmd-config.c:402 gphoto2/gphoto2-cmd-config.c:465
msgid "Value: "
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:364
msgid "Yes"
msgstr ""

#: gphoto2/gphoto2-cmd-config.c:364
msgid "No"
msgstr ""

#: gphoto2/main.c:224
#, c-format
msgid "Zero padding numbers in file names is only possible with %%n."
msgstr ""

#: gphoto2/main.c:233
#, c-format
msgid "You cannot use %%n zero padding without a precision value!"
msgstr ""

#: gphoto2/main.c:266
#, c-format
msgid "The filename provided by the camera ('%s') does not contain a suffix!"
msgstr ""

#: gphoto2/main.c:335
#, c-format
msgid "Invalid format '%s' (error at position %i)."
msgstr ""

#: gphoto2/main.c:390 gphoto2/main.c:596
#, c-format
msgid "Skip existing file %s\n"
msgstr ""

#: gphoto2/main.c:402
#, c-format
msgid "File %s exists. Overwrite? [y|n] "
msgstr ""

#: gphoto2/main.c:414
#, c-format
msgid "Specify new filename? [y|n] "
msgstr ""

#: gphoto2/main.c:426
#, c-format
msgid "Enter new filename: "
msgstr ""

#: gphoto2/main.c:432
#, c-format
msgid "Saving file as %s\n"
msgstr ""

#: gphoto2/main.c:634
msgid "Permission denied"
msgstr ""

#: gphoto2/main.c:796
msgid "Could not trigger capture."
msgstr ""

#: gphoto2/main.c:826
#, c-format
msgid "New file is in location %s%s%s on the camera\n"
msgstr ""

#: gphoto2/main.c:843 gphoto2/main.c:874
#, c-format
msgid "Keeping file %s%s%s on the camera\n"
msgstr ""

#: gphoto2/main.c:864
#, c-format
msgid "Deleting file %s%s%s on the camera\n"
msgstr ""

#: gphoto2/main.c:907
#, c-format
msgid "Event FOLDER_ADDED %s/%s during wait, ignoring.\n"
msgstr ""

#: gphoto2/main.c:917
#, c-format
msgid "Event UNKNOWN %s during wait, ignoring.\n"
msgstr ""

#: gphoto2/main.c:923
#, c-format
msgid "Unknown event type %d during bulb wait, ignoring.\n"
msgstr ""

#: gphoto2/main.c:941
msgid "Could not get capabilities?"
msgstr ""

#: gphoto2/main.c:949
#, c-format
msgid "Time-lapse mode enabled (interval: %ds).\n"
msgstr ""

#: gphoto2/main.c:952
#, c-format
msgid "Standing by waiting for SIGUSR1 to capture.\n"
msgstr ""

#: gphoto2/main.c:958
#, c-format
msgid "Bulb mode enabled (exposure time: %ds).\n"
msgstr ""

#: gphoto2/main.c:971
#, c-format
msgid "Capturing frame #%d...\n"
msgstr ""

#: gphoto2/main.c:973
#, c-format
msgid "Capturing frame #%d/%d...\n"
msgstr ""

#: gphoto2/main.c:983
#, c-format
msgid "Could not set bulb capture, result %d."
msgstr ""

#: gphoto2/main.c:997
msgid "Could not end capture (bulb mode)."
msgstr ""

#: gphoto2/main.c:1010
msgid "Could not trigger image capture."
msgstr ""

#: gphoto2/main.c:1017
msgid "Could not capture image."
msgstr ""

#: gphoto2/main.c:1024
#, c-format
msgid "Capture failed (auto-focus problem?)...\n"
msgstr ""

#: gphoto2/main.c:1035
msgid "Could not capture."
msgstr ""

#: gphoto2/main.c:1069
#, c-format
msgid "Waiting for next capture slot %ld seconds...\n"
msgstr ""

#: gphoto2/main.c:1078 gphoto2/main.c:1119
#, c-format
msgid "Awakened by SIGUSR1...\n"
msgstr ""

#: gphoto2/main.c:1091
#, c-format
msgid "not sleeping (%ld seconds behind schedule)\n"
msgstr ""

#: gphoto2/main.c:1234
#, c-format
msgid "ERROR: "
msgstr ""

#: gphoto2/main.c:1257
#, c-format
msgid ""
"\n"
"Aborting...\n"
msgstr ""

#: gphoto2/main.c:1263
#, c-format
msgid "Aborted.\n"
msgstr ""

#: gphoto2/main.c:1268
#, c-format
msgid ""
"\n"
"Cancelling...\n"
msgstr ""

#: gphoto2/main.c:1419
#, c-format
msgid ""
"Use the following syntax a:b=c:d to treat any USB device detected as a:b as "
"c:d instead. a b c d should be hexadecimal numbers beginning with '0x'.\n"
msgstr ""

#: gphoto2/main.c:1599
msgid "gphoto2 has been compiled without support for CDK."
msgstr ""

#: gphoto2/main.c:1869
#, c-format
msgid "Operation cancelled.\n"
msgstr ""

#: gphoto2/main.c:1873
#, c-format
msgid ""
"*** Error: No camera found. ***\n"
"\n"
msgstr ""

#: gphoto2/main.c:1875
#, c-format
msgid ""
"*** Error (%i: '%s') ***       \n"
"\n"
msgstr ""

#: gphoto2/main.c:1880
#, c-format
msgid ""
"For debugging messages, please use the --debug option.\n"
"Debugging messages may help finding a solution to your problem.\n"
"If you intend to send any error or debug messages to the gphoto\n"
"developer mailing list <gphoto-devel@lists.sourceforge.net>, please run\n"
"gphoto2 as follows:\n"
"\n"
msgstr ""

#: gphoto2/main.c:1901
#, c-format
msgid ""
"Please make sure there is sufficient quoting around the arguments.\n"
"\n"
msgstr ""

#: gphoto2/main.c:1968
msgid "Print complete help message on program usage"
msgstr ""

#: gphoto2/main.c:1970
msgid "Print short message on program usage"
msgstr ""

#: gphoto2/main.c:1972
msgid "Turn on debugging"
msgstr ""

#: gphoto2/main.c:1974
msgid "Set debug level [error|debug|data|all]"
msgstr ""

#: gphoto2/main.c:1976
msgid "Name of file to write debug info to"
msgstr ""

#: gphoto2/main.c:1976 gphoto2/main.c:1981 gphoto2/main.c:1987
#: gphoto2/main.c:2116
msgid "FILENAME"
msgstr ""

#: gphoto2/main.c:1978
msgid "Quiet output (default=verbose)"
msgstr ""

#: gphoto2/main.c:1980
msgid "Hook script to call after downloads, captures, etc."
msgstr ""

#: gphoto2/main.c:1987
msgid "Specify device port"
msgstr ""

#: gphoto2/main.c:1989
msgid "Specify serial transfer speed"
msgstr ""

#: gphoto2/main.c:1989
msgid "SPEED"
msgstr ""

#: gphoto2/main.c:1991
msgid "Specify camera model"
msgstr ""

#: gphoto2/main.c:1991
msgid "MODEL"
msgstr ""

#: gphoto2/main.c:1993
msgid "(expert only) Override USB IDs"
msgstr ""

#: gphoto2/main.c:1993
msgid "USBIDs"
msgstr ""

#: gphoto2/main.c:1999
msgid "Display version and exit"
msgstr ""

#: gphoto2/main.c:2001
msgid "List supported camera models"
msgstr ""

#: gphoto2/main.c:2003
msgid "List supported port devices"
msgstr ""

#: gphoto2/main.c:2005
msgid "Display the camera/driver abilities in the libgphoto2 database"
msgstr ""

#: gphoto2/main.c:2012
msgid "Configure"
msgstr ""

#: gphoto2/main.c:2015
msgid "List configuration tree"
msgstr ""

#: gphoto2/main.c:2017
msgid "Dump full configuration tree"
msgstr ""

#: gphoto2/main.c:2019
msgid "Get configuration value"
msgstr ""

#: gphoto2/main.c:2021
msgid "Set configuration value or index in choices"
msgstr ""

#: gphoto2/main.c:2023
msgid "Set configuration value index in choices"
msgstr ""

#: gphoto2/main.c:2025
msgid "Set configuration value"
msgstr ""

#: gphoto2/main.c:2027
msgid "Reset device port"
msgstr ""

#: gphoto2/main.c:2033
msgid "Keep images on camera after capturing"
msgstr ""

#: gphoto2/main.c:2035
msgid "Keep RAW images on camera after capturing"
msgstr ""

#: gphoto2/main.c:2037
msgid "Remove images from camera after capturing"
msgstr ""

#: gphoto2/main.c:2039
msgid "Wait for event(s) from camera"
msgstr ""

#: gphoto2/main.c:2039 gphoto2/main.c:2041 gphoto2/main.c:2067
msgid "COUNT, SECONDS, MILLISECONDS or MATCHSTRING"
msgstr ""

#: gphoto2/main.c:2041
msgid "Wait for event(s) from the camera and download new images"
msgstr ""

#: gphoto2/main.c:2044
msgid "Capture a quick preview"
msgstr ""

#: gphoto2/main.c:2047
msgid "Show a quick preview as Ascii Art"
msgstr ""

#: gphoto2/main.c:2049
msgid "Set bulb exposure time in seconds"
msgstr ""

#: gphoto2/main.c:2049 gphoto2/main.c:2053
msgid "SECONDS"
msgstr ""

#: gphoto2/main.c:2051
msgid "Set number of frames to capture (default=infinite)"
msgstr ""

#: gphoto2/main.c:2051
msgid "COUNT"
msgstr ""

#: gphoto2/main.c:2053
msgid "Set capture interval in seconds"
msgstr ""

#: gphoto2/main.c:2055
msgid "Reset capture interval on signal (default=no)"
msgstr ""

#: gphoto2/main.c:2057
msgid "Capture an image"
msgstr ""

#: gphoto2/main.c:2059
msgid "Trigger capture of an image"
msgstr ""

#: gphoto2/main.c:2061
msgid "Capture an image and download it"
msgstr ""

#: gphoto2/main.c:2063
msgid "Capture a movie"
msgstr ""

#: gphoto2/main.c:2063
msgid "COUNT or SECONDS"
msgstr ""

#: gphoto2/main.c:2065
msgid "Capture an audio clip"
msgstr ""

#: gphoto2/main.c:2067
msgid "Wait for shutter release on the camera and download"
msgstr ""

#: gphoto2/main.c:2069
msgid "Trigger image capture"
msgstr ""

#: gphoto2/main.c:2075
msgid "List folders in folder"
msgstr ""

#: gphoto2/main.c:2077
msgid "List files in folder"
msgstr ""

#: gphoto2/main.c:2079
msgid "Create a directory"
msgstr ""

#: gphoto2/main.c:2079 gphoto2/main.c:2081
msgid "DIRNAME"
msgstr ""

#: gphoto2/main.c:2081
msgid "Remove a directory"
msgstr ""

#: gphoto2/main.c:2083
msgid "Display number of files"
msgstr ""

#: gphoto2/main.c:2085
msgid "Get files given in range"
msgstr ""

#: gphoto2/main.c:2085 gphoto2/main.c:2089 gphoto2/main.c:2094
#: gphoto2/main.c:2101 gphoto2/main.c:2107 gphoto2/main.c:2112
msgid "RANGE"
msgstr ""

#: gphoto2/main.c:2087
msgid "Get all files from folder"
msgstr ""

#: gphoto2/main.c:2089
msgid "Get thumbnails given in range"
msgstr ""

#: gphoto2/main.c:2092
msgid "Get all thumbnails from folder"
msgstr ""

#: gphoto2/main.c:2094
msgid "Get metadata given in range"
msgstr ""

#: gphoto2/main.c:2096
msgid "Get all metadata from folder"
msgstr ""

#: gphoto2/main.c:2098
msgid "Upload metadata for file"
msgstr ""

#: gphoto2/main.c:2101
msgid "Get raw data given in range"
msgstr ""

#: gphoto2/main.c:2104
msgid "Get all raw data from folder"
msgstr ""

#: gphoto2/main.c:2107
msgid "Get audio data given in range"
msgstr ""

#: gphoto2/main.c:2110
msgid "Get all audio data from folder"
msgstr ""

#: gphoto2/main.c:2112
msgid "Delete files given in range"
msgstr ""

#: gphoto2/main.c:2114
msgid "Delete all files in folder (--no-recurse by default)"
msgstr ""

#: gphoto2/main.c:2116
msgid "Upload a file to camera"
msgstr ""

#: gphoto2/main.c:2118
msgid "Specify a filename or filename pattern"
msgstr ""

#: gphoto2/main.c:2118
msgid "FILENAME_PATTERN"
msgstr ""

#: gphoto2/main.c:2120
msgid "Specify camera folder (default=\"/\")"
msgstr ""

#: gphoto2/main.c:2120
msgid "FOLDER"
msgstr ""

#: gphoto2/main.c:2122
msgid "Recursion (default for download)"
msgstr ""

#: gphoto2/main.c:2124
msgid "No recursion (default for deletion)"
msgstr ""

#: gphoto2/main.c:2126
msgid "Process new files only"
msgstr ""

#: gphoto2/main.c:2128
msgid "Overwrite files without asking"
msgstr ""

#: gphoto2/main.c:2130
msgid "Skip existing files"
msgstr ""

#: gphoto2/main.c:2136
msgid "Send file to stdout"
msgstr ""

#: gphoto2/main.c:2138
msgid "Print filesize before data"
msgstr ""

#: gphoto2/main.c:2140
msgid "List auto-detected cameras"
msgstr ""

#: gphoto2/main.c:2144 gphoto2/shell.c:138
msgid "Show EXIF information of JPEG images"
msgstr ""

#: gphoto2/main.c:2147 gphoto2/shell.c:132
msgid "Show image information, like width, height, and capture time"
msgstr ""

#: gphoto2/main.c:2149
msgid "Show camera summary"
msgstr ""

#: gphoto2/main.c:2151
msgid "Show camera driver manual"
msgstr ""

#: gphoto2/main.c:2153
msgid "About the camera driver manual"
msgstr ""

#: gphoto2/main.c:2155
msgid "Show storage information"
msgstr ""

#: gphoto2/main.c:2157
msgid "gPhoto shell"
msgstr ""

#: gphoto2/main.c:2163
msgid "Common options"
msgstr ""

#: gphoto2/main.c:2165
msgid "Miscellaneous options (unsorted)"
msgstr ""

#: gphoto2/main.c:2167
msgid "Get information on software and host system (not from the camera)"
msgstr ""

#: gphoto2/main.c:2169
msgid "Specify the camera to use"
msgstr ""

#: gphoto2/main.c:2171
msgid "Camera and software configuration"
msgstr ""

#: gphoto2/main.c:2173
msgid "Capture an image from or on the camera"
msgstr ""

#: gphoto2/main.c:2175
msgid "Downloading, uploading and manipulating files"
msgstr ""

#: gphoto2/range.c:104 gphoto2/range.c:158
#, c-format
msgid ""
"%s\n"
"Image IDs must be a number greater than zero."
msgstr ""

#: gphoto2/range.c:110 gphoto2/range.c:164
#, c-format
msgid ""
"%s\n"
"Image ID %i too high."
msgstr ""

#: gphoto2/range.c:126
#, c-format
msgid ""
"%s\n"
"Ranges must be separated by ','."
msgstr ""

#: gphoto2/range.c:140
#, c-format
msgid ""
"%s\n"
"Ranges need to start with a number."
msgstr ""

#: gphoto2/range.c:180
#, c-format
msgid ""
"%s\n"
"Unexpected character '%c'."
msgstr ""

#: gphoto2/range.c:204
#, c-format
msgid ""
"%s\n"
"Decreasing ranges are not allowed. You specified a range from %i to %i."
msgstr ""

#: gphoto2/shell.c:65
#, c-format
msgid "*** Error (%i: '%s') ***"
msgstr ""

#: gphoto2/shell.c:121
msgid "Change to a directory on the camera"
msgstr ""

#: gphoto2/shell.c:122 gphoto2/shell.c:124 gphoto2/shell.c:135
#: gphoto2/shell.c:136
msgid "directory"
msgstr ""

#: gphoto2/shell.c:123
msgid "Change to a directory on the local drive"
msgstr ""

#: gphoto2/shell.c:125 gphoto2/shell.c:156 gphoto2/shell.c:157
msgid "Exit the gPhoto shell"
msgstr ""

#: gphoto2/shell.c:126
msgid "Download a file"
msgstr ""

#: gphoto2/shell.c:126 gphoto2/shell.c:127 gphoto2/shell.c:129
#: gphoto2/shell.c:131 gphoto2/shell.c:133 gphoto2/shell.c:134
#: gphoto2/shell.c:139
msgid "[directory/]filename"
msgstr ""

#: gphoto2/shell.c:127
msgid "Upload a file"
msgstr ""

#: gphoto2/shell.c:128
msgid "Download a thumbnail"
msgstr ""

#: gphoto2/shell.c:130
msgid "Download raw data"
msgstr ""

#: gphoto2/shell.c:134
msgid "Delete"
msgstr ""

#: gphoto2/shell.c:135
msgid "Create Directory"
msgstr ""

#: gphoto2/shell.c:136
msgid "Remove Directory"
msgstr ""

#: gphoto2/shell.c:141 gphoto2/shell.c:158
msgid "Displays command usage"
msgstr ""

#: gphoto2/shell.c:142 gphoto2/shell.c:158
msgid "[command]"
msgstr ""

#: gphoto2/shell.c:143
msgid "List the contents of the current directory"
msgstr ""

#: gphoto2/shell.c:144
msgid "[directory/]"
msgstr ""

#: gphoto2/shell.c:145
msgid "List configuration variables"
msgstr ""

#: gphoto2/shell.c:146
msgid "Get configuration variable"
msgstr ""

#: gphoto2/shell.c:146
msgid "name"
msgstr ""

#: gphoto2/shell.c:147 gphoto2/shell.c:149
msgid "Set configuration variable"
msgstr ""

#: gphoto2/shell.c:147 gphoto2/shell.c:149
msgid "name=value"
msgstr ""

#: gphoto2/shell.c:148
msgid "Set configuration variable index"
msgstr ""

#: gphoto2/shell.c:148
msgid "name=valueindex"
msgstr ""

#: gphoto2/shell.c:150
msgid "Capture a single image"
msgstr ""

#: gphoto2/shell.c:151
msgid "Capture a single image and download it"
msgstr ""

#: gphoto2/shell.c:152
msgid "Capture a preview image"
msgstr ""

#: gphoto2/shell.c:153
msgid "Wait for an event"
msgstr ""

#: gphoto2/shell.c:153 gphoto2/shell.c:154 gphoto2/shell.c:155
msgid "count or seconds"
msgstr ""

#: gphoto2/shell.c:154
msgid "Wait for images to be captured and download it"
msgstr ""

#: gphoto2/shell.c:155
msgid "Wait for events and images to be captured and download it"
msgstr ""

#: gphoto2/shell.c:480
msgid "Invalid command."
msgstr ""

#: gphoto2/shell.c:489
#, c-format
msgid "The command '%s' requires an argument."
msgstr ""

#: gphoto2/shell.c:542
msgid "Invalid path."
msgstr ""

#: gphoto2/shell.c:588
msgid "Could not find home directory."
msgstr ""

#: gphoto2/shell.c:597
#, c-format
msgid "Could not change to local directory '%s'."
msgstr ""

#: gphoto2/shell.c:600
#, c-format
msgid "Local directory now '%s'."
msgstr ""

#: gphoto2/shell.c:638
#, c-format
msgid "Remote directory now '%s'."
msgstr ""

#: gphoto2/shell.c:854
#, c-format
msgid "set-config needs a second argument.\n"
msgstr ""

#: gphoto2/shell.c:875
#, c-format
msgid "set-config-value needs a second argument.\n"
msgstr ""

#: gphoto2/shell.c:896
#, c-format
msgid "set-config-index needs a second argument.\n"
msgstr ""

#: gphoto2/shell.c:948
#, c-format
msgid "Command '%s' not found. Use 'help' to get a list of available commands."
msgstr ""

#: gphoto2/shell.c:955
#, c-format
msgid "Help on \"%s\":"
msgstr ""

#: gphoto2/shell.c:957
#, c-format
msgid "Usage:"
msgstr ""

#: gphoto2/shell.c:960
#, c-format
msgid "Description:"
msgstr ""

#: gphoto2/shell.c:962
#, c-format
msgid "* Arguments in brackets [] are optional"
msgstr ""

#: gphoto2/shell.c:983
#, c-format
msgid "Available commands:"
msgstr ""

#: gphoto2/shell.c:988
#, c-format
msgid "To get help on a particular command, type in 'help command-name'."
msgstr ""
